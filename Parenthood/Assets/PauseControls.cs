﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseControls : MonoBehaviour
{
    [SerializeField] GameObject pause, buildUI;

    public void PauseGame(){
        pause.SetActive(true);
        Raycast.Instance.PauseGame();
    }

    public void ResumeGame(){
        pause.SetActive(false);
        buildUI.SetActive(false);
        Raycast.Instance.ResumeGame();
    }

    public void QuitGame(){
        Application.Quit();
    }


    void Update(){
        if (Input.GetKeyDown(KeyCode.Escape)){
            if(!Static.gamePaused){
                PauseGame();
            } else {
                ResumeGame();
            }
        }
    }

}
