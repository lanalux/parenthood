﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CursorControls : MonoBehaviour
{
    public static CursorControls Instance {get;set;}

    [SerializeField] Text cursorText;

    void Awake(){
        if(Instance == null){
            Instance = this;
        }
    }

    public void ShowText(string message){

        cursorText.text = message;
    }

    public void HideText(){
        cursorText.text = "";
    }

}
