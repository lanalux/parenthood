﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ButtonType {Play, Diaper, Feed};

public class StatButton : MonoBehaviour
{
    public ButtonType buttonType;
    public float increaseAmount;

    AudioSource clickSFX;
    Animator animator;

    float maxValue=100f;

    void Start(){
        clickSFX = this.GetComponent<AudioSource>();
        animator = this.GetComponent<Animator>();
    }

    public void PressButton(){
        animator.SetTrigger("ButtonPress");
        switch(buttonType){
            case ButtonType.Play:
            
                if (Baby.Instance.stats[2].value+increaseAmount > maxValue){
                    Baby.Instance.stats[2].value = maxValue;
                } else {
                    Baby.Instance.stats[2].value += increaseAmount;
                }
                break;
            case ButtonType.Diaper:
                if (Baby.Instance.stats[3].value+increaseAmount > maxValue){
                    Baby.Instance.stats[3].value = maxValue;
                } else {
                    Baby.Instance.stats[3].value += increaseAmount;
                }
                break;
            case ButtonType.Feed:
                if (Baby.Instance.stats[1].value+increaseAmount > maxValue){
                    Baby.Instance.stats[1].value = maxValue;
                } else {
                    Baby.Instance.stats[1].value += increaseAmount;
                }
                break;
        }
        clickSFX.Play();
    }
}
