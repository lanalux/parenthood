﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldCanvasControls : MonoBehaviour
{
    [SerializeField] Transform player;

    void Update(){
        this.transform.LookAt(player);
    }
}
