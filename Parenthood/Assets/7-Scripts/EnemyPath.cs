﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyPath : MonoBehaviour
{
   
   
   public float damage = 30.0f;
   public float health = 30.0f;
   public float reward = 5.0f;
   NavMeshAgent agent;

   bool dead = false;

   void Start() {
   }

   public void SetUpNavMesh(Transform target){
      agent = GetComponent<NavMeshAgent>();
      agent.destination = target.position; 
   }

   void Update() {
      if(health<=0 && !dead){
         dead=true;
         this.GetComponents<AudioSource>()[1].Play();
         Static.numOfEnemies--;
         Static.currentMoney+=reward;
         foreach(Transform child in this.transform){
            child.gameObject.SetActive(false);
         }
         agent.enabled=false;
         StartCoroutine(Destroy());
      }
   }


   IEnumerator Destroy(){
      yield return new WaitForSeconds(1.2f);
      Destroy(this.gameObject);
   }

}
