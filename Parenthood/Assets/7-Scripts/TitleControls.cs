﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleControls : MonoBehaviour
{

    AudioSource clickSFX;

    [SerializeField] GameObject tutorialUI, infoUI;

    void Start(){
        clickSFX=this.GetComponent<AudioSource>();
        Cursor.visible=true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void StartGame(){
        clickSFX.Play();
        StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene(){
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene("Level1");
    }


    public void QuitGame(){
        Application.Quit();
    }

    public void Info(){
        infoUI.SetActive(true);
    }

    public void Tutorial(){
        tutorialUI.SetActive(true);
    }

    public void Close(){
        infoUI.SetActive(false);
        tutorialUI.SetActive(false);
    }

}
