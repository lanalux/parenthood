﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneySpawner : MonoBehaviour
{
    [SerializeField] GameObject moneyPrefab;
    [SerializeField] Transform coinParent;

    float minX = -35.0f;
    float maxX = 35.0f;
    float minZ = -110.0f;
    float maxZ = 8.0f;


    [SerializeField] int maxCoins = 40;
    [SerializeField] float timeToSpawn = 4.0f;
    float timer=0;

    float coinY = 0.8f;


    void Start(){
        timer=timeToSpawn;
    }

    void Update(){
        if (!Static.gamePaused){
            if (timer>=0){
                timer-=Time.deltaTime;
            } else {
                if (coinParent.childCount<maxCoins){
                    GenerateCoin();
                }
            }
        }
    }

    void GenerateCoin(){
        float randX = Random.Range(minX, maxX);
        float randZ = Random.Range(minZ, maxZ);
        GameObject newCoin = Instantiate(moneyPrefab, coinParent);
        newCoin.transform.position = new Vector3(randX,coinY,randZ);
        timer=timeToSpawn;
    }


    

}
