﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerCollider : MonoBehaviour
{
    [SerializeField] Tower tower;
    void OnTriggerEnter(Collider col){
        if(col.gameObject.layer==10)
            tower.enemiesInSight.Add(col.gameObject);
    }

    void OnTriggerExit(Collider col){
        if(col.gameObject.layer==10)
            tower.enemiesInSight.Remove(col.gameObject);
    }
}
