﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Baby : MonoBehaviour
{
    public static Baby Instance {get;set;}

    bool fadeout=false;
    bool waitToTitle=false;
    [SerializeField] CanvasGroup overlay;

    [SerializeField] Text moneyText;





    [System.Serializable]
    public class Stat {
        public string name;
        public float value;
        public RectTransform fill;
        public RectTransform fill2;
        public float decreaseRate;
        public bool inDanger;
        public Renderer sign;
        public Material onMat;
        public Material offMat;
        [HideInInspector] public float rectWidth;
        [HideInInspector] public float rectHeight;

        [HideInInspector] public float rectWidth2;
        [HideInInspector] public float rectHeight2;
    }

    public List<Stat> stats = new List<Stat>();

    float fadeTime=1.0f;
    float timer;
    float fadeSpeed=2f;

    float dangerZone=40f;

    void Awake(){
        if(Instance==null){
            Instance=this;
        }
    }

    void Start(){
        foreach(Stat stat in stats){
            stat.rectWidth = stat.fill.sizeDelta.x;
            stat.rectHeight = stat.fill.sizeDelta.y;
            
            stat.rectWidth2 = stat.fill2.sizeDelta.x;
            stat.rectHeight2 = stat.fill2.sizeDelta.y;
        }

    }

    void Update(){
        foreach (Stat stat in stats){
            if (stat.inDanger && stat.value>dangerZone){
                stat.sign.material=stat.offMat;
            } else if (!stat.inDanger && stat.value<=dangerZone) {
                stat.sign.material=stat.onMat;
            }
            if(stat.value<=0 && !Static.gamePaused){
                stat.value=0;
                GameOver();
            }
        }
        if (fadeout){
            if(timer>0){
                timer -= Time.deltaTime;
                overlay.alpha = 1f-timer;
            } else {
                fadeout=false;
                timer=fadeTime;
                waitToTitle=true;
            }
        }
        if (waitToTitle){
            if(timer>0){
                timer -= Time.deltaTime;
            } else {
                waitToTitle=false;
                SceneManager.LoadScene("Title");
            }
        }

        // Decrease Stats over time
        foreach (Stat stat in stats){
            stat.value -= Time.deltaTime*stat.decreaseRate;
        }
        UpdateHUD();

    }

    void GameOver(){
        CursorControls.Instance.HideText();
        timer=fadeTime;
        Static.gamePaused=true;
        fadeout=true;
    }

    void OnTriggerEnter(Collider collider){
        if(collider.gameObject.layer==LayerMask.NameToLayer("Enemy")){
            this.GetComponent<AudioSource>().Play();
            stats[0].value -= collider.gameObject.GetComponent<EnemyPath>().damage;
            Static.numOfEnemies--;
            Destroy(collider.gameObject);
        }
    }

    public void UpdateHUD(){
        foreach (Stat stat in stats){
            float newWidth = stat.rectWidth*stat.value*0.01f;
            stat.fill.sizeDelta = new Vector2(newWidth, stat.rectHeight);
            float newWidth2 = stat.rectWidth2*stat.value*0.01f;
            stat.fill2.sizeDelta = new Vector2(newWidth2, stat.rectHeight2);
        }
        moneyText.text = "$"+Static.currentMoney.ToString("F0");
    }

}
