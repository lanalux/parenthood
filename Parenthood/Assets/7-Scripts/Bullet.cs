﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public GameObject enemy;

    Vector3 startPos;
    float speed = 25f;
    public float damage = 5f;

    public void StartShooting(GameObject target){
        enemy = target;
    }

    void Update(){
        if(enemy!=null){
            
            this.transform.position = Vector3.MoveTowards(this.transform.position, enemy.transform.position, Time.deltaTime*speed);
            if(Vector3.Distance(this.transform.position, enemy.transform.position) <0.01f){
                enemy.GetComponent<EnemyPath>().health-=damage;
                Destroy(this.gameObject);
            }
        }
    }
}
