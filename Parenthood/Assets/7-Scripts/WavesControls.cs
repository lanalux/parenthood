﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class WavesControls : MonoBehaviour
{
    [System.Serializable]
    public class Waves
    {
        public string name;
        public float timeBetweenEnemies;
        public float timeBetweenNextWave;
        public List<int> enemyNum = new List<int>();

    }

    public List<GameObject> enemies = new List<GameObject>();

    public List<Waves> waves = new List<Waves>();

    enum WaveState{WaveIsComing, WaitingForWave, WaitingForWin, ShowingWonScreen, Won};
    WaveState waveState=WaveState.WaveIsComing;

    [SerializeField] CanvasGroup winUI;

    [SerializeField] Text winText, waveNum;

    [SerializeField] Transform enemyStartPoint, baby;
    public int currentWave=0;
    int currentEnemy=0;
    bool waveIsComing=false;
    bool waitingForWave=false;


    float timeToNextWave = 3.0f;
    float timeToFade = 1.0f;
    float timer=0;

    List<string> winOptions = new List<string>(){
        "doctor",
        "librarian",
        "beat boxer",
        "professional eSports player",
        "cyborg vigilante",
        "plumber",
        "teacher",
        "accountant",
        "tour guide",
        "President of the United States",
        "robotics engineer",
        "pilot",
        "dentist",
        "plumber",
        "game developer",
        "barista",
        "voice actor",
        "meditation guru",
        "stay home parent",
        "beach bum",
        "dolphin trainer",
        "professional wrestler",
        "model",
        "influencer"


    };



    void Start(){
        StartNextWave();
    }

    void Update(){
        switch(waveState){
            case WaveState.WaveIsComing:
                if(currentEnemy<waves[currentWave].enemyNum.Count){
                    if(timer<=0){
                        CreateEnemy(enemies[waves[currentWave].enemyNum[currentEnemy]]);
                        currentEnemy++;
                        Static.numOfEnemies++;
                        timer=waves[currentWave].timeBetweenEnemies;
                    } else {
                        timer-=Time.deltaTime;
                    }
                } else {
                    
                    currentWave++;
                    currentEnemy=0;
                    timer=waves[currentWave].timeBetweenNextWave;
                    waveState=WaveState.WaitingForWave;
                }
                break;
            case WaveState.WaitingForWave:
                if(timer<=0){
                    waitingForWave=false;
                    StartNextWave();
                } else {
                    timer-=Time.deltaTime;
                }
                break;
            case WaveState.WaitingForWin:
                if (Static.numOfEnemies>=0){
                    CursorControls.Instance.HideText();
                    timer=timeToFade;
                    waveState=WaveState.ShowingWonScreen;
                    SetWinText();
                }
                break;
            case WaveState.ShowingWonScreen:
                if (timer<=0){
                    CursorControls.Instance.HideText();
                    timer=timeToFade;
                    waveState=WaveState.Won;
                } else {
                    timer-=Time.deltaTime;
                    
                    winUI.alpha = 1f-timer;
                }
                break;
            case WaveState.Won:
                if (timer<=0){
                    SceneManager.LoadScene("Title");
                } else {
                    timer-=Time.deltaTime;
                }
                break;
        }
    }

    void StartNextWave(){
        if(currentWave<waves.Count-1){
            timer = waves[currentWave].timeBetweenEnemies;
            waveState = WaveState.WaveIsComing;
            waveNum.text = "Wave: " + currentWave.ToString();
        } else {
            waveState = WaveState.WaitingForWin;
        }
    }

    void CreateEnemy(GameObject enemy){
        GameObject newEnemy = Instantiate(enemy, enemyStartPoint);
        newEnemy.transform.localPosition = Vector3.zero;
        newEnemy.GetComponent<EnemyPath>().SetUpNavMesh(baby);

    }


    void SetWinText(){
        int randNum = Random.Range(0,winOptions.Count);
        winText.text = "Congratulations. Your baby grew up to be a " + winOptions[randNum] + "!";
    }

}
