﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class TowerStats{
    public string name;
    [TextArea] public string description;
    public Sprite sprite;
    public GameObject go;
    public float price;
}

public class BuildControls : MonoBehaviour
{
    public static BuildControls Instance {get; set;}

    public List<TowerStats> allTowers = new List<TowerStats>();

    [SerializeField] GameObject towerUI, towerUIPrefab;
    [SerializeField] Text currentMoney;

    public Transform currentTowerBase, towerUIParent;

    Base baseScript;

    AudioSource clickSFX;

    void Awake(){
        if(Instance==null){
            Instance=this;
        }
        clickSFX = this.GetComponent<AudioSource>();
    }


    public void OpenBuildTower(Transform newBase){
        currentMoney.text = "$" + Static.currentMoney.ToString("F0");
        foreach(Transform child in towerUIParent){
            Destroy(child.gameObject);
        }
        currentTowerBase = newBase;
        baseScript = currentTowerBase.GetComponent<Base>();
        Raycast.Instance.PauseGame();
        towerUI.SetActive(true);
        UpdateTowerList();
        
        clickSFX.Play();
    }

    public void CloseBuildUI(){
        Raycast.Instance.ResumeGame();
        towerUI.SetActive(false);
        clickSFX.Play();
    }

    public void BuildThisTower(int towerNum){
        if (Static.currentMoney>=allTowers[towerNum].price){
            CloseBuildUI();
            baseScript.hasTower=true;
            baseScript.towerNum=towerNum;
            foreach (Transform child in currentTowerBase){
                Destroy(child.gameObject);
            }
            GameObject newTower = Instantiate(allTowers[towerNum].go, currentTowerBase);
            newTower.transform.localPosition = Vector3.zero;
            Static.currentMoney-=allTowers[towerNum].price;
            Baby.Instance.UpdateHUD();
            clickSFX.Play();
        }
    }

    public void UpdateTowerList(){
        for(int i=0; i<allTowers.Count; i++){
            GameObject newTowerUI = Instantiate(towerUIPrefab, towerUIParent);
            // make it match the stats
            int _i = i;
            newTowerUI.transform.GetChild(0).GetComponent<Image>().sprite = allTowers[i].sprite;
            newTowerUI.transform.GetChild(1).GetComponent<Text>().text = allTowers[i].name;
            newTowerUI.transform.GetChild(3).GetComponent<Text>().text = allTowers[i].description;
            newTowerUI.transform.GetChild(4).GetChild(0).GetComponent<Text>().text = "Buy for $" + allTowers[i].price.ToString("F0");
            if ((Static.currentMoney>=allTowers[i].price && baseScript.hasTower && baseScript.towerNum!=i) ||(Static.currentMoney>=allTowers[i].price && !baseScript.hasTower)){
                newTowerUI.transform.GetChild(4).GetComponent<Button>().onClick.AddListener(()=>{
                    BuildThisTower(_i);
                });
            } else {
                newTowerUI.transform.GetChild(4).GetComponent<Button>().interactable=false;
            }
        }
    }

}
