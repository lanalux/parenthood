﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;


public class Raycast : MonoBehaviour
{
    public static Raycast Instance {get;set;}

    LayerMask mask = 1<<11 | 1 << 12;
    float viewDistance = 5.0f;
    FirstPersonController fpc;

    void Awake(){
        if(Instance==null){
            Instance=this;
        }
    }

    void Start(){
        fpc = this.transform.parent.GetComponent<FirstPersonController>();
    }


    void Update(){
        if(!Static.gamePaused){
            Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f,0));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, viewDistance, mask)){
                // show curosr
                GameObject currentTarget = hit.transform.gameObject;

                if (currentTarget.tag == "Base"){
                    CursorControls.Instance.ShowText("BUILD TOWER");
                    if(Input.GetMouseButtonDown(0)){
                        BuildControls.Instance.OpenBuildTower( hit.transform);
                    }
                } else if (currentTarget.tag == "Button"){
                    
                    StatButton buttonScript = currentTarget.GetComponent<StatButton>();
                    if (buttonScript){
                        CursorControls.Instance.ShowText("PRESS BUTTON");
                        if(Input.GetMouseButtonDown(0)){
                            buttonScript.PressButton();

                            // TODO: Button press animation
                        }
                    }
                } else {
                    CursorControls.Instance.HideText();
                }
            } else {
                CursorControls.Instance.HideText();
            }
        }

    }



    public void PauseGame(){
        CursorControls.Instance.HideText();
        fpc.enabled=false;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Time.timeScale=0;
        Static.gamePaused=true;
    }

    

    public void ResumeGame(){
        fpc.enabled=true;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Time.timeScale=1.0f;
        Static.gamePaused=false;
    }

}
