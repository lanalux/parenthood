﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public List<GameObject> enemiesInSight = new List<GameObject>();

    public float fireRate=0.50f;
    float timer;
    bool waitingToShoot=false;

    [SerializeField] GameObject enemy, bullet, towerBarrel;

    List<GameObject> bullets = new List<GameObject>();

    void Update(){
        if(enemiesInSight.Count>0){
            if(!waitingToShoot){
                timer=fireRate;
                waitingToShoot=true;
                Shoot(enemiesInSight[0]);
            } else {
                if(timer>0){
                    timer-=Time.deltaTime;
                } else {
                    waitingToShoot=false;
                }
            }
            if(enemiesInSight[0]==null){
                foreach(GameObject bullet in bullets){
                    Destroy(bullet);
                }
                bullets.Clear();
                RemoveFromList();
            }
        }
    }

    void Shoot(GameObject target){
        // instanciate bullet
        this.GetComponent<AudioSource>().Play();
        GameObject newBullet = Instantiate(bullet, towerBarrel.transform);
        newBullet.transform.localPosition = Vector3.zero;
        bullets.Add(newBullet);
        newBullet.GetComponent<Bullet>().StartShooting(target);
    }

    public void RemoveFromList(){
        List<GameObject> tempList = new List<GameObject>();
        for(int i=1;i<enemiesInSight.Count;i++){
            tempList.Add(enemiesInSight[i]);
        }
        enemiesInSight.Clear();
        foreach(GameObject j in tempList){
            enemiesInSight.Add(j);
        }
    }

}
