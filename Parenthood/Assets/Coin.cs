﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    float coinAmount = 2f;
    void OnTriggerEnter(Collider col){
        if(col.gameObject.layer == LayerMask.NameToLayer("Player")){
            this.GetComponent<AudioSource>().Play();
            Static.currentMoney+=coinAmount;
            this.transform.parent.GetChild(0).gameObject.SetActive(false);
            this.GetComponent<Collider>().enabled=false;
            StartCoroutine(CoinPickup());
        }
    }

    IEnumerator CoinPickup() {
        yield return new WaitForSeconds(1.0f);
        Destroy(this.transform.parent.gameObject);
    }
}
